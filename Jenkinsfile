pipeline {
	agent any
	environment {
		APT_LOCK	= "apt-lock-${env.NODE_NAME}"
		SRC_DIR		= "zephyr"
		SDK_VERSION	= "0.10.0"
		SDK_FILE	= "zephyr-sdk-${env.SDK_VERSION}-setup.run"
		SDK_URL		= "https://github.com/zephyrproject-rtos/sdk-ng/releases/download/v${env.SDK_VERSION}/${env.SDK_FILE}"
		ZEPHYR_TOOLCHAIN_VARIANT	= "zephyr"
		ZEPHYR_SDK_INSTALL_DIR		= "${env.HOME}/opt/zephyr-sdk-${env.SDK_VERSION}"
	}
	stages {
		stage('Setup') {
			steps {
				lock("${env.APT_LOCK}") {
					echo 'Setting up dependencies ...'
					sh 'sudo ./setup.sh'
				}
				echo 'Setting up SDK ...'
				sh "if [ ! -e $SDK_FILE ]; then wget $SDK_URL; fi"
				sh 'chmod +x $SDK_FILE'
				sh """
				if [ ! -d $ZEPHYR_SDK_INSTALL_DIR ]; then
					mkdir -p $ZEPHYR_SDK_INSTALL_DIR
					./$SDK_FILE --quiet --noprogress -- -d $ZEPHYR_SDK_INSTALL_DIR
				fi
				"""
			}
		}
		stage('Fetch') {
			steps {
				echo 'Fetching ...'
				sh './fetch.sh'
			}
		}
		stage('Build') {
			steps {
				dir("${env.SRC_DIR}") {
					sh 'pip3 install --user -r scripts/requirements.txt'
					sh '''#!/bin/bash
					. ./zephyr-env.sh
					'''
				}
			}
		}
	}
	post {
		always {
			commonStepNotification()
		}
	}
}
