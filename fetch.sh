#!/bin/bash

d=zephyr
if [ -d $d ]; then
	cd $d
	git pull origin master
	exit 0
fi
git clone https://github.com/zephyrproject-rtos/zephyr $d
