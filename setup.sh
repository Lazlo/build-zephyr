#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs git cmake ninja-build gperf"
pkgs="$pkgs ccache dfu-util device-tree-compiler wget"
pkgs="$pkgs python3-pip python3-setuptools"
pkgs="$pkgs python3-wheel xz-utils file make gcc gcc-multilib"

apt-get -q install --no-install-recommends -y $pkgs
